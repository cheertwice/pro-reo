import type { Metadata, Viewport } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import React from "react";
import { ChakraProvider } from "@chakra-ui/react";
import Navbar from "@/components/Navbar";
import Footer from "@/components/Footer";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  metadataBase: new URL("https://www.pro-reo.cz/"),
  title: "Pro Reo z.s.",
  description:
    "Pro Reo z.s. poskytuje přednášky a semináře na právní témata se zaměřením na dědické a vlastnické právo, určené pro obce a organizace v České republice.",
  keywords: [
    "Pro Reo",
    "právní vzdělávání",
    "dědické právo",
    "vlastnické právo",
    "obce",
    "organizace",
    "nezisková organizace",
    "právní povědomí",
    "právní semináře",
    "právní poradenství",
    "české právo",
    "občanská práva",
    "vzdělávací akce",
    "právní aktuality",
    "právní pomoc",
    "zvyšování právní gramotnosti",
    "komunitní právní služby",
    "legislativa",
    "občanské záležitosti",
    "právní reformy",
    "advokacie",
    "právní normy",
    "spravedlnost",
    "rovné příležitosti",
    "veřejná správa",
    "zákonná práva",
    "ochrana práv",
    "zastupování zájmů",
    "občanské iniciativy",
    "zákony ČR",
    "právní informace",
  ],
  applicationName: "Pro Reo z.s.",
  authors: [{ name: "Tým Pro Reo z.s.", url: "https://www.pro-reo.cz/" }],
  generator: "Next.js",
  icons: [{ rel: "icon", url: "/proreo-logo.png" }],
  openGraph: {
    type: "website",
    url: "https://www.pro-reo.cz/",
    title: "Pro Reo z.s. - Právní vzdělávání pro obce a organizace",
    description:
      "Pro Reo z.s. poskytuje přednášky a semináře na právní témata se zaměřením na dědické a vlastnické právo, určené pro obce a organizace v České republice.",
    siteName: "Pro Reo z.s.",
    images: [{ url: "/proreo-logo.png" }],
  },
  twitter: {
    card: "summary_large_image",
    site: "@ProReo",
    title: "Pro Reo z.s. - Právní vzdělávání pro obce a organizace",
    description:
      "Pro Reo z.s. poskytuje přednášky a semináře na právní témata se zaměřením na dědické a vlastnické právo, určené pro obce a organizace v České republice.",
    images: "/proreo-logo.png",
  },
  manifest: "/manifest.json",
  formatDetection: { telephone: false },
};

const viewport: Viewport = {
  width: "device-width",
  initialScale: 1,
  themeColor: [
    { color: "#3b82f6", media: "(prefers-color-scheme: light)" },
    { color: "#1e40af", media: "(prefers-color-scheme: dark)" },
  ],
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="cs">
      <body className={inter.className}>
        <ChakraProvider>
          <Navbar />
          <main>{children}</main>
          <Footer />
        </ChakraProvider>
      </body>
    </html>
  );
}
