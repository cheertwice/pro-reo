import HeroSection from "@/components/HeroSection";
import AboutUsSection from "@/components/AboutUsSection";
import ContactUsSection from "@/components/ContactUsSection";
import OurTeamSection from "@/components/OurTeamSection";

export default function Home() {
  return (
    <div className={"space-y-12"}>
      <HeroSection />
      <AboutUsSection />
      <OurTeamSection />
      <ContactUsSection />
    </div>
  );
}
