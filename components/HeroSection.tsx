import React from "react";
import Image from "next/image";
import Link from "next/link";
import { TEXTS } from "@/constants";

const HeroSection: React.FC = () => {
  return (
    <div
      id="hero-section"
      className="flex flex-wrap bg-gray-100 py-8 min-h-[calc(100vh-300px)] space-y-8"
    >
      <div className="flex flex-col items-center justify-center w-full lg:w-1/2 space-y-8 px-4 lg:px-0">
        <h1 className="text-3xl lg:text-4xl text-center">
          {TEXTS.hero.header}
        </h1>
        <p className="text-justify w-full lg:w-5/6">
          {TEXTS.hero.text}
        </p>
        <Link
          href="/#kontaktujte-nas"
          className="bg-blue-500 text-white font-bold py-3 px-6 rounded-lg hover:bg-blue-700 transition duration-300 ease-in-out transform hover:-translate-y-1 shadow-lg"
        >
          Kontaktujte nás
        </Link>
      </div>
      <div className="flex justify-center items-center w-full lg:w-1/2 px-4 lg:px-0">
        <Image
          src="/hero-image.jpg"
          alt="Přednášející"
          width={485}
          height={485}
          style={{ objectFit: "cover" }}
          className="rounded-lg shadow-xl max-w-full h-auto"
        />
      </div>
    </div>
  );
};

export default HeroSection;
