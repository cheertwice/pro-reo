import React from "react";
import Link from "next/link";
import Image from "next/image";
import logo from "../public/proreo-logo.svg";

const Footer = () => {
  return (
    <footer className="bg-white border-t-2 border-gray-100 mt-8">
      <div className="container mx-auto px-4 py-12">
        <div className="flex flex-wrap justify-between items-center">
          <div className="flex flex-col lg:flex-row items-center lg:space-x-6 mb-6 lg:mb-0 w-full lg:w-auto">
            <Link href="/" className="flex items-center justify-center">
              <Image src={logo} alt="Pro Reo logo" width={60} height={60} />
              <span className="ml-3 text-xl font-semibold">Pro Reo</span>
            </Link>
          </div>
          <div className="w-full lg:w-2/3 xl:w-1/2 flex flex-col md:flex-row justify-between items-center">
            <div className="flex flex-col space-y-4 md:space-y-0 md:flex-row md:space-x-8 mb-6 md:mb-0">
              <Link
                href="/#o-nas"
                className="text-gray-600 hover:text-gray-900"
              >
                O nás
              </Link>
              <Link
                href="/#kontaktujte-nas"
                className="text-gray-600 hover:text-gray-900"
              >
                Kontakt
              </Link>
              <Link
                href="/#nas-tym"
                className="text-gray-600 hover:text-gray-900"
              >
                Náš Tým
              </Link>
            </div>
            <div className="text-center md:text-right">
              <p className="text-gray-600">
                info@pro-reo.cz | +420 725 830 397
              </p>
              <p className="text-gray-600">
                © {new Date().getFullYear()} Pro Reo, z.s. Všechna práva
                vyhrazena.
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
