"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import logo from "../public/proreo-logo.svg";
import {
  useDisclosure,
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
} from "@chakra-ui/react";
import { FaBars, FaTimes } from "react-icons/fa";
import { NAV_LINKS } from "@/constants";
import { useRouter } from "next/navigation";

const Navbar = () => {
  const router = useRouter();
  const [activeSection, setActiveSection] = useState<string>("");
  const [disableScrollUpdate, setDisableScrollUpdate] = useState(false);
  const {
    isOpen: isDrawerOpen,
    onOpen: onDrawerOpen,
    onClose: onDrawerClose,
  } = useDisclosure();

  const handleLinkClick = (
    e: React.MouseEvent<HTMLElement>,
    href: string,
  ) => {
    if (href.startsWith("/#") || href.startsWith("#")) {
      e.preventDefault();
      const hash = href.split("#")[1];
      setActiveSection(hash); // Update the active section state
      setDisableScrollUpdate(true); // Disable scroll updates temporarily
      const element = document.getElementById(hash);
      if (element) {
        const navbarHeight = 100;
        const elementPosition =
          element.getBoundingClientRect().top + window.scrollY - navbarHeight;
        window.scrollTo({ top: elementPosition, behavior: "smooth" });

        // Re-enable scroll updates after a delay
        setTimeout(() => {
          setDisableScrollUpdate(false);
        }, 1000); // Adjust the timeout to match the scroll duration
      }
    } else {
      // Use Next.js router for other links
      e.preventDefault(); // Prevent default link behavior
      router.push(href);
    }

    onDrawerClose();
  };

  useEffect(() => {
    const handleScroll = () => {
      if (disableScrollUpdate) return; // Skip scroll updates if disabled

      let foundSection = "";
      NAV_LINKS.forEach(({ href }) => {
        const hash = href.split("#")[1];
        const element = document.getElementById(hash);
        if (element) {
          const elementTop = element.getBoundingClientRect().top;
          if (elementTop < window.innerHeight / 2) {
            // Adjust as needed
            foundSection = hash;
          }
        }
      });
      setActiveSection(foundSection);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [disableScrollUpdate]);

  return (
    <div className="flex justify-between items-center w-full h-20 px-4 text-black bg-white nav top-0 sticky z-50">
      <div className="flex ml-0 md:ml-10">
        <a
          href="/"
          onClick={(e) => handleLinkClick(e, "/")}
          className={`flex items-center text-3xl`}
        >
          <Image
            className="mr-3"
            src={logo}
            alt="Pro Reo logo"
            width={50}
            height={50}
          />
          Pro Reo
        </a>
      </div>

      <div
        onClick={isDrawerOpen ? onDrawerClose : onDrawerOpen}
        className="cursor-pointer pr-4 z-10 text-gray-500 md:hidden"
      >
        {isDrawerOpen ? <FaTimes size={30} /> : <FaBars size={30} />}
      </div>

      <ul className="hidden md:flex items-center space-x-4 mr-10">
        {NAV_LINKS.map(({ key, href, label }) => (
          <a
            key={key}
            href={href}
            onClick={(e) => handleLinkClick(e, href)}
            className={`hover:scale-110 ${activeSection === href.split("#")[1]
                ? "font-extrabold"
                : "font-medium"}`
            }
          >
            {label}
          </a>
        ))}
      </ul>

      {/* Mobile drawer */}
      <Drawer
        isOpen={isDrawerOpen}
        placement="right"
        onClose={onDrawerClose}
        size="xs"
      >
        <DrawerOverlay />
        <DrawerContent bg="gray.50" boxShadow="xl">
          <DrawerCloseButton size="lg" />
          <DrawerHeader
            borderBottomWidth="1px"
            fontSize="lg"
            fontWeight="bold"
            bg="blue.500"
            color="white"
          >
            Navigace
          </DrawerHeader>
          <DrawerBody>
            <ul className="space-y-4 mt-4">
              {NAV_LINKS.map(({ key, href, label, icon: Icon }) => (
                <li
                  key={key}
                  className={`flex items-center rounded-md p-4 space-x-4 transition-colors duration-300 hover:cursor-pointer hover:border-2 hover:border-black ${activeSection === href.split("#")[1] ? "bg-blue-500" : "bg-white"}`}
                  onClick={(event) => handleLinkClick(event, href)}
                >
                  <Icon
                    className={`w-6 h-6 ${activeSection === href.split("#")[1] ? "text-white" : "text-blue-500"}`}
                  />
                  <p className={`text-xl ${activeSection === href.split("#")[1] ? "text-white" : "text-black"}`}>
                    {label}
                  </p>
                </li>
              ))}
            </ul>
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </div>
  );
};

export default Navbar;
