import React from "react";
import Image from "next/image";
import { FaRegHandshake, FaBalanceScale, FaBookReader } from 'react-icons/fa';
import {TEXTS} from "@/constants";


const AboutUsSection: React.FC = () => {
    return (
        <div id="o-nas" className="flex flex-wrap bg-white">
            <div className="flex justify-center items-center w-full lg:w-1/2 px-4 lg:px-0 order-2 lg:order-1 lg:mt-0 mt-8">
                <Image
                    src="/about-us-image.jpg"
                    alt="Náš tým"
                    width={600}
                    height={600}
                    style={{ objectFit: "cover" }}
                    className="rounded-lg shadow-xl max-w-full h-auto mt-10 md:mt-0"
                />
            </div>
            <div className="flex flex-col items-center justify-center w-full lg:w-1/2 space-y-6 px-4 lg:px-0 order-1 lg:order-2">
                <h2 className="text-4xl font-semibold text-center lg:text-left">
                    Kdo jsme
                </h2>
                <p className="text-lg text-justify w-full lg:w-5/6">
                    {TEXTS.aboutUs.text}                </p>
                <div className="flex flex-col lg:flex-row justify-center items-center space-y-4 lg:space-y-0 lg:space-x-4 w-full lg:w-5/6">
                    <div className="flex flex-col items-center justify-center space-y-2">
                        <FaRegHandshake size="3em" className="text-blue-500" />
                        <h3 className="text-xl font-semibold">Spolupráce</h3>
                        <p className="text-sm text-center">
                            {TEXTS.aboutUs.cooperation}
                        </p>
                    </div>
                    <div className="flex flex-col items-center justify-center space-y-2">
                        <FaBalanceScale size="3em" className="text-green-500" />
                        <h3 className="text-xl font-semibold">Spravedlnost</h3>
                        <p className="text-sm text-center">
                            {TEXTS.aboutUs.justice}
                        </p>
                    </div>
                    <div className="flex flex-col items-center justify-center space-y-2">
                        <FaBookReader size="3em" className="text-orange-500" />
                        <h3 className="text-xl font-semibold">Vzdělání</h3>
                        <p className="text-sm text-center">
                            {TEXTS.aboutUs.education}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AboutUsSection;
