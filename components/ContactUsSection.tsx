import React from "react";
import { MdEmail, MdPhone } from 'react-icons/md';
import Link from "next/link";

const ContactUsSection: React.FC = () => {
    return (
        <div id="kontaktujte-nas" className="bg-white ">
            <div className="container mx-auto px-4">
                <div className="text-center mb-8">
                    <h2 className="text-4xl font-semibold">Kontaktujte nás</h2>
                    <p className="mt-4 text-lg">
                        Máte-li jakékoli dotazy, neváhejte nás kontaktovat.
                    </p>
                </div>
                <div className="flex flex-wrap justify-center items-center mb-8 w-full mx-auto xl:w-1/2 md:space-x-10 md:space-y-0 space-y-6">
                    <Link href="mailto:info@vasedomena.cz" className="flex w-full md:w-72">
                        <div className="flex items-center justify-center w-16 bg-blue-500 text-white p-4 rounded-l-lg">
                            <MdEmail size="2em" />
                        </div>
                        <div className="flex items-center justify-center w-full bg-white p-4 rounded-r-lg shadow-lg">
                            info@pro-reo.cz
                        </div>
                    </Link>
                    <Link href="tel:+420123456789" className="flex w-full md:w-72">
                        <div className="flex items-center justify-center w-16 bg-green-500 text-white p-4 rounded-l-lg">
                            <MdPhone size="2em" />
                        </div>
                        <div className="flex items-center justify-center w-full bg-white p-4 rounded-r-lg shadow-lg">
                            +420 725 830 397
                        </div>
                    </Link>
                </div>
                <div className="w-full lg:w-3/4 xl:w-1/2 mx-auto">
                    <div className="bg-white p-8 rounded-lg shadow-lg">
                        <form>
                            <div className="grid grid-cols-1 gap-4 mb-4">
                                <div>
                                    <label htmlFor="contact" className="block text-sm font-medium text-gray-700">
                                        Email nebo telefon
                                    </label>
                                    <input type="text" id="contact" name="contact" placeholder="Váš email nebo telefon" className="mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:ring-blue-500 focus:border-blue-500 p-2" required />
                                </div>
                                <div>
                                    <label htmlFor="message" className="block text-sm font-medium text-gray-700">
                                        Vaše zpráva
                                    </label>
                                    <textarea id="message" name="message" rows={4} placeholder="Zde napište vaši zprávu..." className="mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:ring-blue-500 focus:border-blue-500 p-1" required></textarea>
                                </div>
                            </div>
                            <button type="submit" className="w-full bg-blue-500 text-white font-bold py-2 px-4 rounded-lg hover:bg-blue-700 transition duration-300 ease-in-out">
                                Odeslat zprávu
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ContactUsSection;
