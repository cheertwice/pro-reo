import React from "react";
import Image from "next/image";
import {TEXTS} from "@/constants";

const TeamSection: React.FC = () => {
    return (
      <div id="nas-tym" className="bg-gray-100 py-10">
        <div className="text-center mb-10">
          <h2 className="text-4xl font-semibold">Náš Tým</h2>
          <p className="mt-4 text-lg">Odborníci s vášní pro právo a vzdělání</p>
        </div>
        <div className="container mx-auto px-4">
          <div className="flex flex-wrap justify-center items-center gap-8">
            {TEXTS.ourTeam.map(({ name, text, imgPath }) => (
              <div key={name} className="w-full md:w-1/2 lg:w-1/3 xl:w-1/4 flex flex-col items-center">
                <Image
                  src={imgPath}
                  alt={name}
                  width={250}
                  height={250}
                  style={{ objectFit: "cover" }}
                  className="rounded-full"
                />
                <h3 className="text-xl font-semibold mt-4">{name}</h3>
                <p className="text-center mt-2">
                    {text}
                </p>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
};

export default TeamSection;
