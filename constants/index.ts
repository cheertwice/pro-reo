import { FaUserFriends, FaInfo, FaEnvelope } from "react-icons/fa";

export const NAV_LINKS = [
  { href: "/#o-nas", key: "about-us", label: "O nás", icon: FaInfo },
  { href: "/#nas-tym", key: "our-team", label: "Náš tým", icon: FaUserFriends },
  // { href: '/historie-prednasek', key: 'lecture-history', label: 'Historie přednášek', icon: FaHistory },
  {
    href: "/#kontaktujte-nas",
    key: "contact-us",
    label: "Kontaktujte nás",
    icon: FaEnvelope,
  },
];

export const TEXTS = {
  hero: {
    header: "Právní přednášky pro obce a organizace",
    text: "Pro Reo z.s. nabízí širokou škálu přednášek na právní témata, se speciálním zaměřením na dědické a vlastnické právo. Naše přednášky jsou ideální pro obce a organizace, které chtějí poskytnout svým občanům přístup k hodnotným právním informacím a podpořit tak právní povědomí ve své komunitě.",
  },
  aboutUs: {
    text: "Jsme nezisková organizace zaměřená na rozvoj právního povědomí a podporu občanských práv v České republice. Naším cílem je poskytovat vzdělání a poradenství, aby každý mohl rozumět a ochraňovat svá práva.",
    cooperation: "Pracujeme s odborníky a komunitami pro lepší budoucnost.",
    justice: "Podporujeme spravedlnost a rovné příležitosti pro všechny.",
    education: "Nabízíme přístupné vzdělávání a informace.",
  },
  ourTeam: [
    {
      name: "Mgr. Jan Ilek",
      text: "Vedoucí sekce Semináře pro veřejnost. Učitel práva na Obchodní akademie Holešovice. Zapálený pro zvyšování právního povědomí.",
      imgPath: "/mgr-jan-ilek.jpg",
    },
    {
      name: "Mgr. Zuzana Vanýsková",
      text: "Konzultantka a koordinátorka odborných praxí na UK. Specializuje se na práva dětí a podporu právního vzdělávání.",
      imgPath: "/mgr-zuzana-vanyskova.png",
    },
  ],
};
